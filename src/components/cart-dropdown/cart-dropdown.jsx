import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { withRouter } from 'react-router-dom'
 
import { selectCartItem } from '../../redux/cart/selector'

import { toggleCart } from '../../redux/cart/action'

import CartItem from '../cart-item/cart-item'
import './cart-dropdown.scss'
import CustomButton from '../custom-button/custom-button'

const CartDropDown = ({ cartItems, history, dispatch }) => (
    <div className='cart-dropdown'>
        <div className='cart-items'>
            {
                cartItems.length ?
                cartItems.map(
                    cartItem => <CartItem key={cartItem.id} item={cartItem} />
                ) :
                <span className='empty-message'>Your cart is empty</span>
            }   
        </div>
        <CustomButton onClick={
            ()=> {
                dispatch(toggleCart())
                history.push('/checkout')
            }
        } >
            GO TO CHECKOUT
        </CustomButton>
    </div>
)

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItem
})

export default withRouter(connect(mapStateToProps)(CartDropDown))