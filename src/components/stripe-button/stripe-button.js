import React from 'react'
import StripeCheckout from 'react-stripe-checkout'
import Swal from 'sweetalert2'

import './stripe-button.scss'

const StripeCheckoutButton = ({price}) => {
    const priceForStripe = price*100
    const publishableKey = 'pk_test_51H9oaHDdpc9SKo7znlW1YDXXO1ZXm2QdWzfYpPzhrX7FdWo9EiU1ClMHFGvp50g2UTOf3CCbqQTDtzCor590bXBF00eQTmX70x'

    const onToken = () => {
        Swal.fire({
            title: 'Thank you',
            text: 'Your payent succesful',
            icon: 'success',
            confirmButtonText: 'OK'
          })
    }

    return (
        <StripeCheckout 
            label='Pay now'
            name='E-commerce public co.ltd.,'
            billingAddress
            shippingAddress
            image='https://svgshare.com/i/CUz.svg'
            description={`Your total is $${price}`}
            amount={priceForStripe}
            panelLabel='Pay now'
            token={onToken}
            stripeKey={publishableKey}
        />
    )
}

export default StripeCheckoutButton