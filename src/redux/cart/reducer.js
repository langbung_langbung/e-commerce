import cartType from './type'
import { addItemCart, removeItemFromCart } from './utils'
const INITIAL_STATE = {
    hidden: true,
    cartItems: []
}

const cartReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type) {
        case cartType.TOGGLE_CART_HIDDEN:
            return {
                ...state,
                hidden: !state.hidden
            }
        case cartType.ADD_ITEM: 
            return {
                ...state,
                cartItems: addItemCart(state.cartItems, action.payload)
            }

        case cartType.CLEAR_ITEM_FORM_CART:
            return {
                ...state,
                cartItems: state.cartItems.filter(cartItem => cartItem.id !== action.payload.id)
            } 
        case cartType.REMOVE_ITEM:
            return {
                ...state,
                cartItems: removeItemFromCart(state.cartItems, action.payload)
            }
        default:
            return state
    }
}

export default cartReducer