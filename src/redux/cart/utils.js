export const addItemCart = (cartItems, cartItemAdd) => {
    const existItem = cartItems.find(
        cartItem => cartItem.id === cartItemAdd.id
    )

    if(existItem) {
        return cartItems.map( 
            cartItem => cartItem.id === cartItemAdd.id ?
                { ...cartItem, quantity: cartItem.quantity + 1 } :
                cartItem 
        )
    }

    return [...cartItems, { ...cartItemAdd, quantity:1 }]
}

export const removeItemFromCart = (cartItems, cartItemToRemove) =>{
    const exitsItem = cartItems.find(
        cartItem => cartItem.id === cartItemToRemove.id
    )
    
    if(exitsItem.quantity === 1)
        return cartItems.filter(
            cartItem => cartItem.id !== cartItemToRemove.id 
        )
    
    return cartItems.map(
        cartItem => cartItem.id === cartItemToRemove.id ?
        { ...cartItem, quantity: cartItem.quantity - 1} :
        cartItem
    ) 
}