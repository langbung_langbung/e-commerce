import React from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import StripeCheckoutButton from '../../components/stripe-button/stripe-button'
import CheckoutItem from '../../components/checkout-item/checkout-item'
import { selectCartTotal, selectCartItem } from '../../redux/cart/selector'
import './checkout.scss'



const Checkout = ({ cartItems, total}) => (
    <div className='checkout-page' >
        <div className='checkout-header'>
            <div className='checkout-block'>
                <span>Product</span>
            </div>
            <div className='checkout-block'>
                <span>Description</span>
            </div>
            <div className='checkout-block'>
                <span>Quantity</span>
            </div>
            <div className='checkout-block'>
                <span>Price</span>
            </div>
            <div className='checkout-block'>
                <span>Remove</span>
            </div>
        </div>
        {
            cartItems.map(
                (cartItem) => <CheckoutItem key={cartItem.id} cartItem={cartItem} />
            )
        }
        <div className='total'>
            ${total}
        </div>
        <StripeCheckoutButton price={total} />
    </div>
)

const mapStateToProps = createStructuredSelector({
    total: selectCartTotal,
    cartItems: selectCartItem
})

export default connect(mapStateToProps)(Checkout)