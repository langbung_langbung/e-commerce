import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyChvz2DP-lh4g1XHxl5V4mPyOTuWQBNp3o",
    authDomain: "e-commerce-9f7a8.firebaseapp.com",
    databaseURL: "https://e-commerce-9f7a8.firebaseio.com",
    projectId: "e-commerce-9f7a8",
    storageBucket: "e-commerce-9f7a8.appspot.com",
    messagingSenderId: "23077917250",
    appId: "1:23077917250:web:1492fa5d493f5eb5a553db"
  }
firebase.initializeApp(config)
export const auth = firebase.auth()
export const firestore = firebase.firestore()

const provider = new firebase.auth.GoogleAuthProvider()
provider.setCustomParameters({'promp':'select_account'})
export const signInWithGoogle = () => {
    auth.signInWithPopup(provider)
    return false
  }

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if(!userAuth) return

  const userRef =  firestore.doc(`users/${userAuth.uid}`)
  const snapshot = await userRef.get()

  if(!snapshot.exists) {
    const {displayName, email } = userAuth
    const createdAt = new Date()
    try {
      userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      })
    } catch (error) {
      console.log('create user error:',error.message)
    }
  }
  return userRef
}
export default firebase